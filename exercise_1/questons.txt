Question #1:

os.environ
You can set (as key:value pair ) environment variables and access them with "os.environ.get".

os.getenv
It is used to get environment variables. It returns the value of the key if it exits.


Question #2:

json.dump():
Serialize obj as a JSON. It is used when the Python objects have to be stored in a file.
Requires json file name as a target. 

json.dumps():
Serialize obj to a JSON formatted str (for printing, parsing)
Slower but does not require any such file name to be passed. ("s" at the end of dumps stands for string)

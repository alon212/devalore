#!/usr/bin/env python3

import itertools
import re


# 1. join list into a bigger list
l = [["name", "dinner"], ["Abraham", "Avi"], ["Hello", "Tree", "Door"], ["Name", "Glass", "Two", "am"]]
bigger = list(itertools.chain.from_iterable(l))
print ("\nUnified into a bigger list:")
print(bigger,"\n\n")

# 2. No duplicates found (case sensetive)


# remove words which has 2 or more letters in the middle
String = ' '.join(bigger)
shortword = re.compile(r'\W*\b\w{1,3}\b')
finalString = (shortword.sub('', String))
print ("Removed words which has 2 or more letters in the middle:")
print(finalString.split(" "),"\n")







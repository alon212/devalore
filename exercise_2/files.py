#!/usr/bin/env python3

import pathlib
from glob import glob
import subprocess
import glob 
import itertools
import os
import pathlib
import re



path = "./main_folder"
jpg = glob.glob(path + "/**/*.jpg", recursive = True)

# paths of images
print ("\n Locatios of images:")
file_path = ('\n'.join(map(str, jpg)))
print (file_path,"\n\n")

# amount of images
how_many_files = len(jpg)
print ("There are", how_many_files, "images in the main folder", "\n\n")

# image file names
print("Images name:")
all_files = ('\n'.join(map(os.path.basename, jpg)))
print (all_files, "\n\n")


folder_names = glob.glob(path + "/*", recursive = True)

output = subprocess.check_output(["find","./main_folder", "-type","d"],universal_newlines=True)
new = output.split("\n")
folder_amount = (len(new[:-1]))
# total number of folders
print ("there are",folder_amount,  "folder and sub-folders in total","\n\n")

# paths to all folders
print ("Full paths to all folder:")
print (output, "\n\n")


# full_path = file_path.split("\n")
# # path_no_file_name = os.path.dirname(full_path[couter])
# # file_name = all_files.split("\n")

# test01 = (os.path.split(full_path[0]))
# print (type(test01))


# 8. Print a filtered list with the name of the folders that contains the letter 'i'
folder_with_i = subprocess.check_output(["find",".", "-type","d", "-name","*i*"],universal_newlines=True)
print (folder_with_i)
